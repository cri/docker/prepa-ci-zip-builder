# Prepa CI ZIP builder

This docker image is used to generate a zip containing all students' code.

## Environement variables

### Required

* `OUTPUT_ZIP`: The path in which the output is located.
* `PROJECT`: The project name.
* `API_SERVICE_ACCOUNT`: The credentials that is used to sign in with the API (format: `username:password`).
* `GIT_USERNAME`: The git username used to clone students' repositories.
* `GIT_KEY`: The ssh key used to clone students' repositories.

### Optional

* `API_ENDPOINT`: The endpoint of the API, defaults to `https://prepa-ci.cri.epita.fr/api`.
* `GIT_SSH_PREFIX`: Then prefix of git, defaults to `git.cri.epita.fr/prepa`.
