FROM alpine

RUN apk add --no-cache zip jq curl

WORKDIR /prepa_ci/zip-builder
COPY command.sh .

CMD ["/prepa_ci/zip-builder/command.sh"]
