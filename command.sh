#!/bin/sh

if [ -z "$GIT_SSH_PREFIX" ]; then
    GIT_SSH_PREFIX='git.cri.epita.fr/prepa'
fi

if [ -z "$API_ENDPOINT" ]; then
    API_ENDPOINT='https://prepa-ci.cri.epita.fr/api'
fi

if [ -z "$OUTPUT_ZIP" ]; then
    echo "no output specified"
    exit 1
fi

if [ -z "$API_SERVICE_ACCOUNT" ]; then
    echo "no credentials specified for the API"
    exit 1
fi

if [ -z "$GIT_KEY" ]; then
    echo "no ssh key specified"
    exit 1
fi

if [ -z "$GIT_USERNAME" ]; then
    echo "no git username specified"
    exit 1
fi

if [ -z "$PROJECT" ]; then
    echo no project specified
    exit 1
fi

members=`curl -s -L -X GET "${API_ENDPOINT}/projects/$project/get_members/" \
        --user "$API_SERVICE_ACCOUNT"`

echo "$GIT_KEY" > ssh_key

cloned=""
for username in `echo $members | jq -r '.[]'`; do
    git clone -i ssh_key "git@$GIT_SSH_PREFIX/$PROJECT"
    if [ "$?" -eq 0 ]; then
        cloned="$cloned $username"
    fi
done

zip -r "$OUTPUT_ZIP" $cloned
